macro(MyMacro myVar)
 set(myVar "newValue")
 message("argument: ${myVar}")
endmacro()

set(myVar "1st value")
message("my var is now ${myVar}")

MyMacro("called value")
message("my var is now ${myVar}")
