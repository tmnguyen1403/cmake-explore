## CMake commands

##
` cmake -S <source-folder> -B buildtree  `

- create buildtree folder providing source
- ignore source will use the current working directory as source

` cmake --build buildtree `
- build project using the buildtree folder

` cmake --build <buildtree> --parallel [<number-of-jobs>]`

` cmake --build <dir> -j [<number-of-jobs>]`
- can be used for parallel build

## Installing
` cmake --install <dir> [<options>]`
- dir is a path to a generated build tree

## Command-line tool
` cmake -E`
- list all available command lines
` cmake -E <command> [<options]`
- run a command
## Cmake debugging
` cmake --help `
- can be used to view a list of available generators on current machine

`cmake --system-information [file]`
- Get general information about variables, commands, macros, and other settings

`cmake --log-level=<level>`
- level: ERROR, WARNING, NOTICE, STATUS, VERBOSE, DEBUG or TRACE
- can set by using CMAKE_MESSAGE_LOG_LEVEL

`cmake --trace`
- print every command with the filename and exact line number it is called from

`cmake --build <dir> --verbose`
- print details while buiding the project
