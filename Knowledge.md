## Listfiles
  Files that contain the CMake language are called listfiles and can be included one in another, by calling `include()`, `find_package()`, or `add_subdirectory()`
1. They have .cmake extension
2. These variables are set as CMake walks the source tree:
   - CMAKE_CURRENT_LIST_DIR
   - CMAKE_CURRENT_LIST_FILE
   -CMAKE_PARENT_LIST_FILE
   - CMAKE_CURRENT_LIST_LINE

`add_subdirectory(api)`
- include another CMakeLists.txt file from the api directory

## Comments
- Single line comment: #
- Multi line comment: #[=[... ]=]. can have zero to many equal signs "=" but the number of equal signs must match

## Comman arguments
1. Bracket arguments
- not evaluated because they are used to pass multiline strings
- Example _**commands/bracket.cmake**_
2. Quoted arguments
- open and closed with "
- can used escape sequence \
- example _**commands/quoted.cmake**_
3. Unquoted arguments
- argum\ ent\;1 #a single argument
- arg;ume nts # three arguments
- example _**commands/unquoted.cmake**_

## Variables
1. `set()`
- ${} reference normal or cache variables
- $ENV{} reference environment variables
- $CACHE{} reference cache variables
- example: _**set.cmake**_
2. Environment variables
- Environment variables set during  configuration stage persisted through the built process
- example _**test_environment**_
3. Cache variables
- persistent variables stored in CMakeCache.txt file in your build tree
- not available in scripts, only exist in projects
- `set(<variable> <value> CACHE <type> <docstring> [FORCE])`
- type: BOOL, FILEPATH, PATH, STRING, INTERNAL
- docstring: a label that will be displayed by the GUI

## Scopes
- Function scope: custom functions defined with `function()` are executed
- Directory scope: when a CMakeLists.txt listfile in a nested directory is executed from the `add_subdirectory()` command

## Lists
- CMake concatenates all elements into a string, using a semiconlon (;)
- `a;list;of;5;elements`
- `set (myList a list of five elements)`
- `set (myList "a;list;of;five;elements")`
- `set (myList a list "of;five;elements")`
1. List commands
- `list(LENGTH <list> <out-var>)`
- `list(GET <list> <element index> [<index> ...] <out-var>)
- `list(JOIN <list> <glue> <out-var>)`
- SUBLIST, FIND, APPEND, FILTER, INSERT, POP_BACK, POP_FRONT
- PREPEND, REMOVE_ITEM, REMOVE_AT
- REMOVE_DUPLICATES, TRANSFORM, REVERSE, SORT

## CONDITIONAL BLOCKS
- `if(condition)
<commands> 
elseif(<condition>)
<commands>
else()
<commands> 
endif()`
- logical operators: NOT, AND, OR
- strings are True: `ON, Y, YES, TRUE, a non-zero number`
- `if(VARIABLE)` - asking whether the variable is defined
- `if(${VARIABLE})` - evaluated the value of the variable first
- if(DEFINED <name>) - check defined variable

## COMPARISION OPERATORS
- EQUAL, LESS, LESS_EQUAL, GREAT, GREAT_EQUAL
- if (1.3.4 VERSION_LESS_EQUAL 1.4) - comparing software version by adding a VERSION_ prefix to any of the operators
- if("A" STREQUAL "${B}") - add STR prefix
- <VARIABLE|STRING> MATHCHES <regex> - any mached groups are captured in CMAKE_MATCH_<n> variables
1. Other cheks
- <VARIABLE|STRING> IN_LIST <VARIABLE>
- <COMMAND> <command-name> - if a command is available for invocation
-POLICY <policy-id> - if a CMake policy exists
-TEST <test-name>
-TARGET <target-name> - if a build target is defined

##  Filesystem Debug
- `EXISTS <path-to-file-or-directory>`
- `<file1> IS_NEWER_THAN <file2>`: return true if file1 is newer than (or equal to) file2 or if one of the two files doesn't exist
- `IS_DIRECTORY path-to-directory`:
- `IS_SYMLINK file-name`: cheks if a path is a symbolic link
- `IS_ABSOLUTE path`:

## Loops
1. while
- `while(<condition>)
 <commands>
 [break()
 continue()]
 endwhile()`
2. foreach
- `foreach(<loop_var> RANGE <max>)
<commands>
endforeach()`: iterate from 0 to max(inclusive)
- `foreach(<loop_var> RANGE <min> <max> [<step>])`: all arguments must be nonnegative integers; <min> has to be smaller than <max>
- `foreach(<loop_var> IN [LISTS <lists>] [ITEMS <items>]): take elements from all the provided <lists>,<items> and store them in <loop var>
- example: _**command/foreach.cmake**_

## COMMAND DEFINITIONS
- using macro() or function()
- ${ARGC}: the count of arguments
- ${ARGV}:  a list of all arguments
- ${ARGO}, ${ARG1}, ${ARG2}: the value of an argument at a specific index
- ${ARGN}: a list of anonymous arguments that were passed by a caller after the last expected argument
1. macro()
- `macro(<name> [<argument>...])
  <commands>
endmacro()
`
- arguments passed to macros aren't treated as real variables but rather as constant find-and-replace instructions
- example _**command/macro.cmake**_
2. function()
- `function(<name> [<argument>...]) 
<commands>
endfunction()`
- if a function call passed more arguments than were declared, the excess arguments will be stored in ARGN variable
- Cmake sets the following variables for each function: CMAKE_CURRENT_FUNCTION,CMAKE_CURRENT_FUNCTION_LIST_DIR, CMAKE_CURRENT_FUNCTION_LIST_FILE,CMAKE_CURRENT_FUNCTION_LIST_LINE
- example _**commands/function.cmake**_ 

## Usefule commands
1. include()
- `include(<file|module> [OPTIONAL][RESULT_VARIABLE <var>])`
- if provided .cmake extension, CMake will try to open and execute it providing no seperate scope (changes to variable will affect the calling scope)
_ OPTIONAL: will not raise error if file not found
- RESULT_VARIABLE: indicate open successful or not

2. include_guard()
- `include_guard([DIRECTORY|GLOBAL])`
- files are included once
- DIRECTORY: provide protection for variables within the current directory
- GLOBAL: applies the protection for the whole build

3. file()
- `file(READ <filename> <out-var> [...])`
- `file({WRITE | APPEND} <filename> <content>...)`
- `file(DOWNLOAD <url> [<file>][...])`
- allow read, write, and transfer files, and work with the filesystem, file locks, paths, and archives, all in a system-independent manner.

4. execute_process()
- `execute_process(COMMAND <cmd1> `[<arguments>]...[OPTIONS])
- run other processes and collect their output
- can run multiple commands by adding more COMMAND
- OPTIONS
  - TIMEOUT <seconds>
  - WORKING_DIRECTORY <directory>
  - RESULTS_VARIABLE <variable>: list of exit codes
  - RESULT_VARIABLE <variable>: single exit code
  - OUTPUT_VARIABLE <variable>: collect output
  - ERROR_VARIABLE <variable>: collect error; can be merge with stdout by using the same variable

## Dependency graph
- `cmake --graphviz=<out_dir>/test.dot <build_tree>`
- The preceeding command generates dependency graph of a Cmake project
- copy the content of test.dot file into GraphvizOnline(https://dreampuf.github.io/GraphvizOnline)



